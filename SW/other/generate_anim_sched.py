
for i in range(20):
  msg='\
#ifdef anime{0}\n\
    case {0}:\n\
      /* Check if animation run or not. If not -> change pointer on following\n\
       * animation\n\
       */\n\
      if( i_stat_cfg_anime.anim_run == 0)\n\
      {{\n\
        // Now read last word and check for "read_end_of_anim" flag\n\
        check_for_read_end_of_anim( &i_anime_repeat_counter );\n\
\n\
        // Test if animation should be played once again or not\n\
        if( i_anime_repeat_counter == 0 )\n\
        {{// If 0 -> Counted out -> play next animation\n\
          // Clear "read_end_of_anim" flag\n\
          i_stat_cfg_anime.read_end_of_anim=0;\n\
#ifdef anime{1}\n\
          p_anime = anime{1};                   // Change pointer\n\
#endif\n\
#ifndef anime{1}\n\
          // If next animation is not defined -> do not set "anim_run" flag\n\
          i_stop_playing = 1;\n\
#endif\n\
          i_anime_scheduler_animation_id++;   // Increase ID to next animation\n\
        }}\n\
        else\n\
        {{\n\
          p_anime = anime{0};\n\
          i_anime_repeat_counter--;\n\
        }}\n\
      }}\n\
      break;\n\
#endif\n\
      /*---------------------------------------------------------------------*/\n\
'.format(i, i+1)
  print(msg)
  


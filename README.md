# ==<| RGB LED cube 5x5x5 |>==
## About
 * This is totally useless device, but it might be nice decoration if you're
   geek enough
 * Cube is autonomous. So simply build it, upload firmware (also set fuses)
   and you're ready to go
 * Optionally you can connect cube to computer and control it from PC

## Download
 * For downloading, please use GIT command with "--recursive" parameter

```bash
   $ git clone --recursive git@gitlab.com:RGB_LED_cube_5x5x5/RGB_LED_cube_5x5x5.git
```

## Update
 * If you already downloaded project, but you want to keep all your sources
   up-to-date, simply run following command:

```bash
$ git submodule update --recursive --remote
```

## Help and support
 * If you have questions, or if you want to contribute on this project, just
   let me know [martin.stej at gmail dot com]


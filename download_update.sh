#!/bin/bash

# Script for updating all sub-modules from remote server
git submodule update --recursive --remote
